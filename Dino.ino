// A simple T-rex Run game based on the 30DLIS kit
// Developed by Ishani and Arijit Sengupta
// Required libraries
/*
  For documentation on u8glib functions:
  https://github.com/olikraus/u8glib/wiki/userreference
*/
#include <U8glib.h>
#include "TM1637Display.h"

/* constants */
// HW Pins required
constexpr unsigned char JU = 3; // RotaryEncoder JU Pin
constexpr unsigned char DU = 2;
// Define the display connections pins:
constexpr unsigned char DCLK = 6;
constexpr unsigned char DIO = 5;
// pin 10 drives the buzzer
constexpr unsigned char  buzzer = 10;
// Sounds we use for the hit effects
constexpr unsigned int jumpSound = 700;
constexpr unsigned char blahSound = 125;
constexpr unsigned int speedSound = 1000;
constexpr unsigned char DBOUNCE = 180;
// Game states
constexpr unsigned char gameStart = 0;
constexpr unsigned char gameEnd = 1;
constexpr unsigned char gamePlaying = 2;
constexpr unsigned char speed = 8;
// Game objects
constexpr unsigned char dinoIndex = 0;
constexpr unsigned char obstacleIndex = 1;
constexpr unsigned char cloudIndex = 2;


// PRE-SAVED BITMAP CONSTANTS
// Create array that turns all segments off:
constexpr unsigned char blank[] = {0x00, 0x00, 0x00, 0x00}; // 0xff is a hexidecimal number whose binary
// representation is all zeros
//20 x 21
constexpr unsigned char dinoJump [] U8G_PROGMEM = {
  0x00, 0xFC, 0x07, 0x00, 0xFE, 0x07, 0x00, 0xEE, 0x0F, 0x00, 0xFE, 0x0F,
  0x00, 0xFE, 0x0F, 0x00, 0xFE, 0x0F, 0x00, 0xFE, 0x07, 0x06, 0xFF, 0x03,
  0xC3, 0xFF, 0x00, 0xE7, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,
  0xFF, 0x3F, 0x00, 0xFE, 0x3F, 0x00, 0xFC, 0x1F, 0x00, 0xF8, 0x1F, 0x00,
  0xF0, 0x1F, 0x00, 0xF0, 0x0E, 0x00, 0x60, 0x0E, 0x00, 0xE0, 0x0E, 0x00,
  0xE0, 0x1E, 0x00,
};
// 20 x 21
constexpr unsigned char dinoSet[] U8G_PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x1C, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
};
//20 x 21
constexpr unsigned char dinoLeft [] U8G_PROGMEM = {
  0x00, 0xFC, 0x07, 0x00, 0xFE, 0x07, 0x00, 0xEE, 0x0F, 0x00, 0xFE, 0x0F,
  0x00, 0xFE, 0x0F, 0x00, 0x7E, 0x08, 0x00, 0x7E, 0x00, 0x06, 0xFF, 0x03,
  0x87, 0x3F, 0x00, 0xE7, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,
  0xFF, 0x3F, 0x00, 0xFE, 0x3F, 0x00, 0xFC, 0x1F, 0x00, 0xF8, 0x1F, 0x00,
  0xF0, 0x1F, 0x00, 0xE0, 0x1E, 0x00, 0x60, 0x00, 0x00, 0xE0, 0x00, 0x00,
  0xE0, 0x00, 0x00,
};
//20 x 21
constexpr unsigned char dinoRight [] U8G_PROGMEM = {
  0x00, 0xFC, 0x07, 0x00, 0xEE, 0x07, 0x00, 0xE6, 0x0F, 0x00, 0xFE, 0x0F,
  0x00, 0xFE, 0x0F, 0x00, 0xFE, 0x0F, 0x00, 0x7C, 0x00, 0x06, 0xFF, 0x03,
  0xC3, 0xFF, 0x00, 0xE7, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,
  0xFF, 0x3F, 0x00, 0xFE, 0x3F, 0x00, 0xFC, 0x1F, 0x00, 0xF8, 0x1F, 0x00,
  0xF0, 0x1F, 0x00, 0xF0, 0x0F, 0x00, 0xE0, 0x0E, 0x00, 0x00, 0x0E, 0x00,
  0x00, 0x1E, 0x00,
};
constexpr unsigned char nothing[] U8G_PROGMEM = {
};
//shape 0:  39 x 12
constexpr unsigned char cloud [] U8G_PROGMEM = {
  0x00, 0x00, 0xC0, 0x00, 0x00, 0x00, 0x00, 0xBE, 0x03, 0x00, 0x00, 0x00,
  0x03, 0x06, 0x00, 0x00, 0x80, 0x01, 0x04, 0x00, 0x00, 0x40, 0x00, 0x1C,
  0x00, 0x00, 0x40, 0x00, 0xE4, 0x03, 0x00, 0x18, 0x00, 0x00, 0x02, 0xE0,
  0x0F, 0x00, 0x00, 0x0E, 0x30, 0x00, 0x00, 0x00, 0x10, 0x10, 0x00, 0x00,
  0x00, 0x20, 0x12, 0x00, 0x00, 0x00, 0x40, 0x03, 0xFF, 0xFF, 0xFF, 0x7F,
};
//shape 1: 10 x 20
constexpr unsigned char oneCactus [] U8G_PROGMEM = {
 0x30, 0x00, 0x78, 0x00, 0x78, 0x00, 0x78, 0x00, 0x78, 0x01, 0xFB, 0x03,
 0xFF, 0x03, 0xFF, 0x03, 0xFF, 0x03, 0xFF, 0x03, 0xFF, 0x03, 0xFF, 0x01,
 0xFE, 0x00, 0x78, 0x00, 0x78, 0x00, 0x78, 0x00, 0x78, 0x00, 0x78, 0x00,
 0x78, 0x00, 0x00, 0x00,
};
//shape 2: 20 x 20
constexpr unsigned char twoCactus [] U8G_PROGMEM = {
 0x30, 0xC0, 0x00, 0x38, 0xE0, 0x00, 0x38, 0xE8, 0x00, 0x38, 0xEC, 0x00,
 0x38, 0xED, 0x04, 0xBB, 0xED, 0x0E, 0xBB, 0xED, 0x0E, 0xBB, 0xFD, 0x0E,
 0xBB, 0xFD, 0x0E, 0xBB, 0xF9, 0x0E, 0xFF, 0xF1, 0x0F, 0xFF, 0xE0, 0x07,
 0x7E, 0xE0, 0x01, 0x38, 0xE0, 0x00, 0x38, 0xE0, 0x00, 0x38, 0xE0, 0x00,
 0x38, 0xE0, 0x00, 0x38, 0xE0, 0x00, 0x38, 0xE0, 0x00, 0x00, 0x00, 0x00,
};

//shape 3: 20 x 20
constexpr unsigned char threeCactus [] U8G_PROGMEM = {
 0x00, 0xC0, 0x00, 0x18, 0xC0, 0x01, 0x18, 0xC0, 0x01, 0x58, 0xD8, 0x01,
 0x58, 0xFC, 0x01, 0x58, 0xFC, 0x0F, 0x78, 0xDC, 0x0F, 0x7F, 0xFC, 0x0F,
 0x3B, 0xFD, 0x0D, 0x1B, 0xF9, 0x0C, 0x5B, 0xF5, 0x0F, 0x5B, 0xC5, 0x07,
 0x5F, 0xE7, 0x03, 0xDE, 0xE7, 0x01, 0xD8, 0xC3, 0x01, 0x98, 0xC1, 0x01,
 0x18, 0xC1, 0x01, 0x18, 0xC1, 0x01, 0x18, 0xE1, 0x01, 0x00, 0x00, 0x00,
};
//shape 4: 6 x 12
constexpr unsigned char oneCactusSmall [] U8G_PROGMEM = {
 0x0C, 0x0C, 0x3C, 0x3D, 0x2D, 0x3D, 0x1D, 0x0E, 0x0C, 0x0C, 0x0C, 0x0C,
};
//shape 5: 12 x 12
constexpr unsigned char twoCactusSmall [] U8G_PROGMEM = {
 0x0C, 0x03, 0x0C, 0x03, 0x6C, 0x0B, 0x6D, 0x0B, 0x6D, 0x0B, 0xBD, 0x0B,
 0x1F, 0x0F, 0x0E, 0x03, 0x0C, 0x03, 0x0C, 0x03, 0x0C, 0x03, 0x0C, 0x03,
};
//shape 6: 17 x 12
constexpr unsigned char threeCactusSmall [] U8G_PROGMEM = {
 0x04, 0x41, 0x00, 0x0C, 0x61, 0x00, 0xFC, 0x79, 0x01, 0xFD, 0x7D, 0x01,
 0x7D, 0x6D, 0x01, 0x7D, 0x7D, 0x01, 0xCF, 0xE5, 0x01, 0xCE, 0x67, 0x00,
 0x8C, 0x67, 0x00, 0x0C, 0x63, 0x00, 0x0C, 0x61, 0x00, 0x0C, 0x61, 0x00,
};
//shape 7:12x12
constexpr unsigned char birdModel[] U8G_PROGMEM = {
 0x00, 0x00, // ............
 0x0C, 0x00, // ....##......
 0x1E, 0x00, // ...####.....
 0x3F, 0x00, // ..######....
 0x7F, 0x80, // .########...
 0xFF, 0xC0, // ##########..
 0xFF, 0xE0, // ###########.
 0xFF, 0xE0, // ###########.
 0x7F, 0x80, // .########...
 0x3F, 0x00, // ..######....
 0x1E, 0x00, // ...####.....
 0x0C, 0x00, // ....##......
};

constexpr unsigned char dinoBlah [] U8G_PROGMEM = {
  0x00, 0xFC, 0x07, 0x00, 0xFE, 0x07, 0x00, 0xC6, 0x0F, 0x00, 0xC6, 0x0F,
  0x00, 0xCE, 0x0F, 0x00, 0xFE, 0x0F, 0x00, 0xFE, 0x0F, 0x06, 0xFF, 0x03,
  0x87, 0x7F, 0x00, 0xE7, 0xFF, 0x00, 0xFF, 0xFF, 0x00, 0xFF, 0xFF, 0x00,
  0xFF, 0x3F, 0x00, 0xFE, 0x3F, 0x00, 0xFC, 0x1F, 0x00, 0xF8, 0x1F, 0x00,
  0xF0, 0x1F, 0x00, 0xF0, 0x0E, 0x00, 0x60, 0x0E, 0x00, 0x60, 0x0E, 0x00,
  0xE0, 0x1E, 0x00,
};

constexpr unsigned char gameOver [] U8G_PROGMEM = {
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0xF8, 0x01, 0x02, 0x0C, 0x0C, 0x3E, 0x00, 0x78, 0x60,
  0x30, 0x7C, 0xF0, 0x01, 0x0C, 0x01, 0x07, 0x14, 0x0A, 0x02, 0x00, 0x84,
  0x40, 0x10, 0x04, 0x10, 0x02, 0x04, 0x00, 0x05, 0x14, 0x0A, 0x02, 0x00,
  0x02, 0x41, 0x10, 0x04, 0x10, 0x02, 0x04, 0x00, 0x05, 0x14, 0x0A, 0x02,
  0x00, 0x02, 0xC1, 0x18, 0x04, 0x10, 0x02, 0xC4, 0x81, 0x0D, 0x34, 0x0B,
  0x3E, 0x00, 0x02, 0x81, 0x08, 0x7C, 0xF0, 0x01, 0x04, 0x81, 0x08, 0x24,
  0x09, 0x02, 0x00, 0x02, 0x81, 0x0D, 0x04, 0x10, 0x01, 0x04, 0x81, 0x0F,
  0x64, 0x09, 0x02, 0x00, 0x02, 0x01, 0x05, 0x04, 0x10, 0x02, 0x0C, 0xC1,
  0x18, 0xC4, 0x08, 0x02, 0x00, 0x84, 0x00, 0x05, 0x04, 0x10, 0x02, 0xF8,
  0x41, 0x10, 0xC4, 0x08, 0x3E, 0x00, 0x78, 0x00, 0x07, 0x7C, 0x10, 0x02,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
  0x00, 0x00, 0x00,
};


// In Addition, this sketch uses the I2C Pins for the U8G Panel
// A1+A2
// Create display object of type TM1637Display
// The display shows the current score
TM1637Display OurDisplay = TM1637Display(DCLK, DIO);
// Create the oled display object - this shows gameplay
// and welcome/win/loss messages
U8GLIB_SH1106_128X64  My_u8g_Panel(U8G_I2C_OPT_NONE); // I2C / TWI

/* Abstract Class */
class GameObject {
  public:
    GameObject(void) = default;
    virtual ~GameObject(void) = default;
    /* purely virtual methods */
    virtual void move(void) = 0;
    virtual void drawObject(void) = 0;
};

class Dinosaur : public GameObject {
  public:
    /* passing reference of the game */
    Dinosaur(volatile int &crouching, int &status, int &move, int &set, volatile int &jump);
    virtual ~Dinosaur(void) = default;
    /* final virtual methods */
    void move(void) final;
    void drawObject(void) final;
  private:
    /* references */
    volatile int &crouch;
    int &gameStatus;
    int &dinoMove;
    int &setmove;
    volatile int &jumping;
};

Dinosaur::Dinosaur(volatile int &crouching, int &status, int &move, int &set, volatile int &jump) : 
  GameObject(), crouch(crouching), gameStatus(status), dinoMove(move), setmove(set), jumping(jump)   
  {/* empty constructor */}

void Dinosaur::move(void) {
  if (jumping == 0 && crouch == 0) {
    dinoMove = (dinoMove + 1) % 3;
    setmove = (setmove + 1) % 3;
  }
  else {
    if (jumping == 1) {
      dinoMove = dinoMove + 8;
      if (dinoMove > 32) { 
        jumping = 2;
      }
      else { /*do nothing */ } 
    }
    else {
      dinoMove = dinoMove - 8;
      if (dinoMove < 8) {
        jumping = 0;
        dinoMove = 0;
      }
      else { /*do nothing */ } 
    }
     
    if (crouch == 1 && jumping == 0) {
      setmove = setmove + 8;
      if (setmove > 32) {
        crouch = 2;
      }
      else { /*do nothing */ } 
    }
    else {
      setmove = setmove - 8;
      if (setmove < 8) {
        crouch = 0;
        setmove = 0;
      }
      else { /*do nothing */ } 
    }
  }
}

void Dinosaur::drawObject (void) {
  if (gameStatus == gameEnd) {
    My_u8g_Panel.drawXBMP( 0, 43 - dinoMove, 20, 21, dinoBlah);
    return;
  }
  else { /*do nothing */ } 

  if (setmove == 0 || setmove == 1 || setmove == 2) {
    switch (dinoMove) {
      case -2:
          My_u8g_Panel.drawXBMP(0, 43, 20, 21, nothing);
          break;
      case -1:
          My_u8g_Panel.drawXBMP(0, 43, 20, 21, dinoBlah);
          break;
      case 0:
          My_u8g_Panel.drawXBMP(0, 43, 20, 21, dinoJump);
          break;
      case 1:
          My_u8g_Panel.drawXBMP(0, 43, 20, 21, dinoLeft);
          break;
      case 2:
          My_u8g_Panel.drawXBMP(0, 43, 20, 21, dinoRight);
          break;
      default:
          My_u8g_Panel.drawXBMP(0, 43 - dinoMove, 20, 21, dinoJump);
          break;
    }
  } 
  else {
      My_u8g_Panel.drawXBMP(0, 43, 20, 21, dinoSet);
  }
}

class Obstacle : public GameObject {
  public:
    /* passing reference of the game */
    Obstacle(int (&obstaclesRef)[2], int (&obstaclexRef)[2]);
    virtual ~Obstacle(void) = default;
    /* final virtual method */
    void move(void) final;
    void drawObject(void) final;
  private:
    void drawShape(int shape, int x);
    /* references */
    int (&obstacles)[2];
    int (&obstaclex)[2];
};

Obstacle::Obstacle(int (&obstaclesRef)[2], int (&obstaclexRef)[2]) : 
  GameObject(), obstacles(obstaclesRef), obstaclex(obstaclexRef) 
  {/* empty constructor */}

void Obstacle::drawShape(int shape, int x) {
  switch (shape) {
    case 1: 
      My_u8g_Panel.drawXBMP( x, 44, 10, 20, oneCactus); 
      break;
    case 2: 
      My_u8g_Panel.drawXBMP( x, 44, 20, 20, twoCactus); 
      break;
    case 3: 
      My_u8g_Panel.drawXBMP( x, 44, 20, 20, threeCactus); 
      break;
    case 4: 
      My_u8g_Panel.drawXBMP( x, 52, 6, 12, oneCactusSmall); 
      break;
    case 5: 
      My_u8g_Panel.drawXBMP( x, 40, 12, 12, birdModel);
      break;
    case 6: 
      My_u8g_Panel.drawXBMP( x, 52, 17, 12, threeCactusSmall); 
      break;
    default:
      /* do nothing */
      break;
  }
}

void Obstacle::move(void) {
  int obx = obstaclex [0];
  obx = obx - speed;
  if (obx < -20) {
    obstaclex[0] = obstaclex[1];
    obstaclex[1] = obstaclex[0] + random(80, 125);
    obstacles[0] = obstacles[1];
    obstacles[1] = random (1, 6);
  }
  else {
    obstaclex[0] = obx;
    obstaclex[1] -= speed;
  }
}

void Obstacle::drawObject(void) {
  drawShape(obstacles[0], obstaclex[0]);
  drawShape(obstacles[1], obstaclex[1]);
}

class Cloud : public GameObject {
  public:
    Cloud(void);
    virtual ~Cloud(void) = default;
    /* final virtual method */
    void move(void) final;
    void drawObject(void) final;
  private:
    int cloudx;
};

Cloud::Cloud(void) : GameObject(), cloudx(128) {/* empty constructor */}

void Cloud::move(void) {
  cloudx --;
  if (cloudx < -38) {
    cloudx = 128;
  }
  else { /* do nothing */ }
}

void Cloud::drawObject(void) {
  My_u8g_Panel.drawXBMP(cloudx, 5, 39, 12, cloud); 
}

/* Main Class of the game */
class Game {
  public:
    Game(void);
    ~Game(void) = default;
    /* main methods */
    void ShowScore(void);
    void resetGame(void);
    void draw(void);
    void checkCollision(void);
    inline int GetStatus(void);
    /* interrupt methods - static */
    static void StartStopJump(void);
    static void StartStopCrouch(void);
    /* public array of constant pointers to objects */
    GameObject * const objects[3];
  private:
    /* private methods */
    void u8g_prepare(void);
    /* private variables */
    int MyScore;
    int lastBeep;
    int dinoMove;
    int setmove;
    int obstacles[2];
    int obstaclex[2];
    unsigned long startTime;
    /* variables used in the static methods */
    static int gameStatus;
    static volatile int crouch;
    static volatile int jumping;
    /* private objects of the game */
    Dinosaur dinosaur;
    Obstacle obstacle;
    Cloud cloud;
};

/* initialization of static variables */
int Game::gameStatus = gameStart;
volatile int Game::crouch = 0;
volatile int Game::jumping = 0;

/* interrupt methods - static */
void Game::StartStopJump (void) {
  static unsigned long last_interrupt = 0;
  if (millis() - last_interrupt > DBOUNCE) {
    if (gameStatus == gamePlaying) {
      if (jumping == 0) {
        jumping = 1;
        tone (buzzer, jumpSound, 100);
      }
      else { /* do nothing */ }
    }
    else if (gameStatus == gameStart) {
      gameStatus = gamePlaying;
    }
    else {
      gameStatus = gameStart;
    }
  }
  else { /* do nothing */ }

  last_interrupt = millis(); //note the last time the ISR was called
}

/* interrupt methods - static */
void Game::StartStopCrouch (void) {
  static unsigned long last_interrupt = 0;
  if (millis() - last_interrupt > DBOUNCE) {
    if (gameStatus == gamePlaying) {
      if (crouch == 0) {
        crouch = 1;
        tone (buzzer, jumpSound, 100);
      }
      else { /* do nothing */ }
    }
    else if (gameStatus == gameStart) {
      gameStatus = gamePlaying;
    }
    else {
      gameStatus = gameStart;
    }
  }
  else { /* do nothing */ }

  last_interrupt = millis(); //note the last time the ISR was called
}

Game::Game(void) : MyScore(0), lastBeep(0), dinoMove(0), setmove(0),
  obstacles{1, 4}, obstaclex{128, 200}, startTime(millis()),
  dinosaur(crouch, gameStatus, dinoMove, setmove, jumping), obstacle(obstacles, obstaclex), cloud(),
  objects{&dinosaur, &obstacle, &cloud} 
  {/* empty constructor */}

// Display the score on the 7seg display
void Game::ShowScore (void) {
  if (gameStatus == gamePlaying) {
    unsigned long curTime = millis ();
    MyScore = (curTime - startTime) * speed / 1000;
    OurDisplay.showNumberDecEx(MyScore);
    if (MyScore / 100 > lastBeep) {
      tone (buzzer, 1000, 100);
      delay (150);
      tone (buzzer, 1250, 100);
      lastBeep = MyScore / 100;
    }
    else { /* do nothing */ }
  }
  else { /* do nothing */ }
}

void Game::resetGame (void) {
  MyScore = 0;
  startTime = millis();
  obstaclex[0] = 128;
  obstaclex[1] = 200;
  dinoMove = 0;
}

void Game::u8g_prepare(void) {
  My_u8g_Panel.setFont(u8g_font_6x10);
  My_u8g_Panel.setFontRefHeightExtendedText();
  My_u8g_Panel.setDefaultForegroundColor();
  My_u8g_Panel.setFontPosTop();
}

void Game::draw(void) {
  u8g_prepare();
  if (gameStatus == gamePlaying) {
    objects[dinoIndex] -> drawObject();
    objects[obstacleIndex] -> drawObject();
    objects[cloudIndex] -> drawObject();
  }
  else if (gameStatus == gameStart) {
    My_u8g_Panel.drawStr( 0, 10, "Welcome to");
    My_u8g_Panel.drawStr( 10, 30, "Dino!!");
    My_u8g_Panel.drawStr( 0, 50, "Push to begin");
    resetGame();
    ShowScore();
  }
  else {
    My_u8g_Panel.drawXBMP( 14, 12, 100, 15, gameOver);
    objects[dinoIndex] -> drawObject();
    objects[obstacleIndex] -> drawObject();
    objects[cloudIndex] -> drawObject();
  }
}

void Game::checkCollision(void) {
  int obx = obstaclex[0];
  int obw, obh, obl;

  switch (obstacles[0]) {
    case 0: 
      obw =  39; 
      obh = 10; 
      obl=10;
      break;
    case 1: 
      obw = 10; 
      obh = 20; 
      obl=20;
      break;
    case 2: 
      obw = 17; 
      obh = 20; 
      obl=20;
      break;
    case 3: 
      obw = 17; 
      obh = 20;
      obl=20; 
      break;
    case 4: 
      obw = 6; 
      obh = 12;
      obl=12; 
      break;
    case 5: 
      obw = 12; 
      obh = 40;
      obl=2; 
      break;
    case 6: 
      obw = 17; 
      obh = 12;
      obl=12; 
      break;
    default:
      /* do nothing */
      break;
  }
  if (obx > 15 || obx + obw < 5 || dinoMove > obh - 3 || (0 < setmove && setmove < obh - obl)) 
  { /* do nothing */ }
  else {
    gameStatus = gameEnd;
    tone (buzzer, 125, 100);
    delay(150);
    tone (buzzer, 125, 100);
  }
}

inline int Game::GetStatus(void) {
  return gameStatus;
}

void setup() {
  OurDisplay.setBrightness(7);
  OurDisplay.clear();
  pinMode(JU, INPUT_PULLUP);
  pinMode(DU, INPUT_PULLUP);
  pinMode(buzzer, OUTPUT);
  attachInterrupt(digitalPinToInterrupt(JU), Game::StartStopJump, FALLING);
  attachInterrupt(digitalPinToInterrupt(DU), Game::StartStopCrouch, FALLING);
}

void loop(void) {
  Game game;
  game.resetGame();

  /* game loop*/
  for(;;) {
    game.ShowScore();
    My_u8g_Panel.firstPage();
    do {
      game.draw();
    } while ( My_u8g_Panel.nextPage() );

    if (game.GetStatus() == gamePlaying) {
      game.objects[dinoIndex] -> move();
      game.objects[obstacleIndex] -> move();
      game.objects[cloudIndex] -> move();
      game.checkCollision();
    }
    else{ /* do nothing */}
  }
}









